ActiveAdmin.register JobPost do
    permit_params :title, :description, :expiry_date
  
    index do
      selectable_column
      id_column
      column :title
      column :description
      column :expiry_date
      column :created_at
      actions
    end

    form do |f|
      f.inputs do
        f.input :title
        f.input :description
      end
      f.actions
    end
    
    show do |job_post|
        attributes_table do
            row :id
            row :title
            row :description
            row :expiry_date
            row :created_at
            row :updated_at
            panel 'Job Applications' do
                table_for job_post.jop_applications.collect {|x| x.job_application} do
                  column ("title") {|app|
                    link_to(app.title, admin_jop_application(app))}
                end
              end
            end
        active_admin_comments
    end 

    controller do
        def create
          super
          @job_post.admin_user = current_admin_user
          @job_post.save
        end
    end
end
  