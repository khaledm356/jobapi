# frozen_string_literal: true

module Api
  module V1
    class JobPostsController < ApplicationController
      before_action :authenticate_seeker
      def index
        authorize! :index, JobPost
        job_posts = JobPost.where(status: 0)  
        render json: job_posts
      end

      def show
        Authorize! :show, JobPost
        job_post = JobPost.find(params[:id])
        render json: job_post
      end
    end
  end
end
