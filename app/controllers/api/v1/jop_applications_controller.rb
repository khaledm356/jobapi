# frozen_string_literal: true

module Api
  module V1
    class JopApplicationsController < ApplicationController
      def create
        authorize! :create, JopApplication
        jop_application = JopApplication.new(jop_application_params)
        jop_application.job_post_id = jop_application_params[:job_post_id]
        jop_application.seeker = current_seeker
        if jop_application.save
          render json: jop_application
        else
          render json: jop_application.errors.full_messages
        end
      end

      def show
        jop_application = JopApplication.find(params[:id])
        authorize! :show, jop_application
        render json: jop_application
      end

      private

      def jop_application_params
        params.permit(:title, :job_post_id)
      end
    end
  end
end
