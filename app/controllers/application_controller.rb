class ApplicationController < ActionController::Base
    before_action :configure_permitted_parameters, if: :devise_controller?
    before_action :authenticate_seeker
    NotAuthorized = Class.new(StandardError)

    private

    def configure_permitted_parameters
        devise_parameter_sanitizer.permit(:sign_up, keys: [:email])
    end

    def authenticate_seeker
        if request.headers['Authorization'].present?
            authenticate_or_request_with_http_token do |token|
                begin
                    jwt_payload = JWT.decode(token, Rails.application.secrets.secret_key_base).first
                    @current_seeker_id = jwt_payload['id']
                rescue JWT::ExpiredSignature, JWT::VerificationError, JWT::DecodeError
                    head :unauthorized
                end
          end
        else
            render json: {}, status: 401
        end
    end

    # def authenticate_seeker!(options = {})
    #     head :unauthorized unless signed_in?
    # end
    
    def current_seeker
        @current_seeker ||= super || Seeker.find(@current_seeker_id)
    end

    def signed_in?
        @current_seeker_id.present?
    end

    def current_ability
        @current_ability ||= Ability.new(current_seeker)
    end
end
