# frozen_string_literal: true

class RegistrationsController < Devise::RegistrationsController
    # skip_before_action :verify_authenticity_token  
    respond_to :json
    
    def create
        build_resource(seeker_params)
        resource_saved = resource.save
        expire_data_after_sign_in!
        render json: resource, status: :created
        rescue ValidationError => e
        clean_up_passwords resource
        set_minimum_password_length
        render json: e.reason, status: :unprocessable_entity
    end
    
    private
    
    def seeker_params
        params.permit(:email, :password, :password_confirmation)
    end
end
