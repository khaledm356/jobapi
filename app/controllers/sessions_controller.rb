# frozen_string_literal: true
# module Api
#   module V1
#     module Seeker
      class SessionsController < Devise::SessionsController
        skip_before_action :verify_authenticity_token
        prepend_before_action :require_no_authentication, :only => [:create]
        respond_to :json
          def create
            seeker = Seeker.find_by_email(sign_in_params[:email])
            
            if seeker && seeker.valid_password?(sign_in_params[:password])
              @current_seeker = seeker
              sign_in(:seeker, @current_seeker, store: false, bypass: false)
              
              render_create_success
            else
              render json: { errors: { 'email or password' => ['is invalid'] } }, status: :unprocessable_entity
            end
          end
          
          private 
          def sign_in_params
            params.permit(:email, :password)
          end

          def render_create_success
            render json: {
              data: @current_seeker.as_json, token: @current_seeker.generate_jwt
            }
          end

          def current_token
            request.env['warden-jwt_auth.token']
          end
        end
          
      # end
#     end
#   end
# end

