json.seeker do |json|
    json.partial! 'seeker/seeker', seeker: current_seeker
end