class CreateJopApplications < ActiveRecord::Migration[6.1]
  def change
    create_table :jop_applications do |t|
      t.references :job_post, null: false, foreign_key: true
      t.references :seeker, null: false, foreign_key: true
      t.string :title
      t.boolean :seen

      t.timestamps
    end
  end
end
