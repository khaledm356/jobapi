# frozen_string_literal: true

FactoryBot.define do
  factory :job_post do
    admin_user
    title       { FFaker::Job.title }
    description { FFaker::Company.position }
  end
end
