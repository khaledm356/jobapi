# frozen_string_literal: true

FactoryBot.define do
  factory :jop_application do
    seeker
    job_post
    title       { FFaker::Job.title }
  end
end
