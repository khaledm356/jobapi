require 'swagger_helper'

RSpec.describe 'api/v1/job_posts', type: :request do
  before do
    @seeker = FactoryBot.create(:seeker)
    @job_post = FactoryBot.create(:job_post)
    @job_application = FactoryBot.create(:jop_application, job_post: @job_post, seeker: @seeker)
    @job_application1 = FactoryBot.create(:jop_application, job_post: @job_post)
    @auth_headers = @seeker.generate_jwt
    @headers = {
      'CONTENT_TYPE' => 'application/json',
      'ACCEPT' => 'application/json',
      'Authorization' => "Bearer #{@auth_headers}",
    }
  end

  path "/api/v1/job_posts/:job_post_id" do
    post 'show job post' do
      tags 'Jop application index'
      consumes 'application/json'
      produces 'application/json'
      response '200', 'job application shown succsessfully' do
        it "should show job application" do
          get "http://localhost:3000/api/v1/job_posts/#{@job_post.id}/jop_applications/#{@job_application.id}", headers: @headers
          expect(response).to have_http_status(:success)
          body = JSON.parse(response.body)
          expect(body["id"]).to eq(@job_application.id)
        end
      end

      response '401', "unauthorized see job application" do
        it "Should not be able to job application" do
            get "http://localhost:3000/api/v1/job_posts/#{@job_post.id}/jop_applications/#{@job_application1.id}"
          expect(response).to have_http_status(401)
        end
      end
    end
  end
end
